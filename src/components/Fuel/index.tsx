import { useEffect, useState } from 'react'
import { FiEdit2 } from 'react-icons/fi'

import { getFuel, updateFuel } from './services'
import * as S from './styles'
import { IFuelState } from './types'

const TIME_TO_UPDATE_MS = 1000

export const Fuel = () => {
  const [fuels, setFuels] = useState<IFuelState[]>()
  const [editMode, setEditMode] = useState(false)

  function toggleEditMode() {
    setEditMode((prev) => !prev)
  }

  const fetchAndUpdateData = async () => {
    const data = await getFuel()

    setFuels(data)
  }

  useEffect(() => {
    fetchAndUpdateData()
  }, [])

  useEffect(() => {
    if (editMode) return

    const intervalId = setInterval(() => {
      fetchAndUpdateData()
    }, TIME_TO_UPDATE_MS)

    return () => {
      clearInterval(intervalId)
    }
  }, [editMode])

  function handleUpdatePrice(fuldId: number, price: string) {
    const updateFuels = fuels?.map((fuel) => {
      if (fuel.id === fuldId) {
        fuel.price = Number(price)
        fuel.updated = true
      }

      return fuel
    })

    setFuels(updateFuels)
  }

  async function handleSavePrice() {
    const changed = fuels?.filter((f) => f.updated === true)

    if (!changed || changed.length === 0) {
      toggleEditMode()
      return
    }

    for (const changedFuel of changed) {
      const { updated, ...rest } = changedFuel

      await updateFuel(rest)
    }

    fetchAndUpdateData()
    toggleEditMode()
  }

  return (
    <S.Container>
      <S.Title>Posto React JS</S.Title>
      <S.SettingsIcon onClick={() => toggleEditMode()} />

      <S.Panel>
        {editMode && (
          <S.InfoText>
            <FiEdit2 />
            Altere o preço dos itens
          </S.InfoText>
        )}

        {fuels?.map((fuel) => (
          <S.Row key={fuel.id}>
            <S.Box>
              <S.FuelText>{fuel.name}</S.FuelText>
            </S.Box>
            <S.Box>
              {editMode ? (
                <S.FuelInput
                  type="number"
                  value={fuel.price}
                  onChange={(event) =>
                    handleUpdatePrice(fuel.id, event.target.value)
                  }
                />
              ) : (
                <S.FuelPrice>{fuel.price}</S.FuelPrice>
              )}
            </S.Box>
          </S.Row>
        ))}

        {editMode && (
          <S.Row>
            <S.SaveButton onClick={handleSavePrice}>
              <S.SaveIcon />
              <span>Save</span>
            </S.SaveButton>
          </S.Row>
        )}
      </S.Panel>
    </S.Container>
  )
}
