import { IFuel } from './types'
import { Api } from '../../services/api'

export const getFuel = async () => {
  const response = await Api.get('fuel')

  return response.data
}

export const updateFuel = async (fuel: IFuel) => {
  const response = await Api.put(`fuel/${fuel.id}`, fuel)

  return response.data
}
