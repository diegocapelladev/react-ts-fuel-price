import styled, { css } from 'styled-components'

import imageGasStation from '../../assets/gas-station.jpg'

import { FiSave, FiSettings } from 'react-icons/fi'
import media from 'styled-media-query'

export const Container = styled.div`
  background-image: url(${imageGasStation});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom;
  height: 100vh;
  width: 100vw;
  position: relative;
`

export const Title = styled.h1`
  ${({ theme }) => css`
    font-weight: bold;
    font-size: ${theme.font.sizes.size48PX};
    color: ${theme.colors.colorGrey200};
    padding-top: 2rem;
    text-align: center;

    ${media.lessThan('medium')`
      font-size: ${theme.font.sizes.size32PX};
      text-align: left;
      padding-left: 2rem;
    `}
  `}
`

export const SettingsIcon = styled(FiSettings)`
  ${({ theme }) => css`
    color: ${theme.colors.colorGrey200};
  `}
  font-size: 3.2rem;
  cursor: pointer;

  position: absolute;
  top: 2rem;
  right: 2rem;
`

export const Panel = styled.section`
  width: 30rem;
  height: 50rem;
  border-radius: 0.5rem;
  position: relative;
  left: calc(100vw - 35rem);
  margin-top: 6rem;
  ${({ theme }) => css`
    background: ${theme.colors.colorGrey100};
  `}
`
export const Box = styled.div`
  width: 13.5rem;
  height: 6.5rem;
  border-radius: 0.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  ${({ theme }) => css`
    background: ${theme.colors.colorBlue200};
  `}
`
export const Row = styled.div`
  display: flex;
  justify-content: space-around;
  padding-top: 1.5rem;
`
export const FuelText = styled.span`
  font-size: 2.5rem;
  font-weight: bold;
  text-align: center;
  ${({ theme }) => css`
    color: ${theme.colors.colorWhite};
  `}
`
export const FuelPrice = styled.span`
  font-family: 'Digital-7 Mono', sans-serif;
  font-size: 6rem;
  text-align: left;
  font-weight: 600;
  ${({ theme }) => css`
    color: ${theme.colors.colorYellow600};
  `}
`

export const InfoText = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 1rem;
    color: ${theme.colors.colorWhite};
  `}
`

export const SaveButton = styled.button`
  font-size: 1.5rem;
  font-weight: bold;
  padding: 1rem;

  text-align: center;
  border-radius: 0.5rem;
  cursor: pointer;
  display: flex;
  align-items: center;
  ${({ theme }) => css`
    background: ${theme.colors.colorBlue200};
    color: ${theme.colors.colorWhite};
  `}

  & > span {
    margin-left: 10px;
  }

  &:hover {
    filter: brightness(0.8)
  }
`

export const SaveIcon = styled(FiSave)`
  ${({ theme }) => css`
    color: ${theme.colors.colorWhite};
    font-size: 3.2rem;
  `}
`

export const FuelInput = styled.input`
  ${({ theme }) => css`
    color: ${theme.colors.colorYellow600};
    font-family: 'Digital-7 Mono', sans-serif;
    font-size: 5rem;
    background: none;
    border: none;
    width: 100%;
    height: 100%;
    text-align: center;
    padding: 0;
  `}

`
