export default {
  grid: {
    container: '112rem',
    gutter: '3.2rem'
  },
  border: {
    radius: '0.6rem'
  },
  font: {
    family:
      "Inter, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
    light: 300,
    regular: 400,
    medium: 500,
    semiBold: 600,
    bold: 700,
    black: 800,
    sizes: {
      size48PX: '4.8rem',
      size32PX: '3.2rem',
      size24PX: '2.4rem',
      size20PX: '2rem',
      size18PX: '1.8rem',
      size16PX: '1.6rem',
      size14PX: '1.4rem',
      size12PX: '1.2rem',
      size10PX: '1rem'
    }
  },
  colors: {
    colorBlue200: '#0B4F6C',
    colorGrey200: '#DCE0D9',
    colorGrey100: 'rgba(176, 178, 184, 0.79)',
    colorWhite: '#FBFBFF',
    colorYellow600: '#FFE548',
    white: '#FFFFFF',
    background: '#FAFAFA',
    card: '#F3F2F2',
    input: '#EDEDED',
    button: '#E6E5E5',
    hover: '#D7D5D5',
    label: '#8D8686',
    text: '#574F4D',
    subtitle: '#403937',
    title: '#272221',
    error: '#f84747',
    purpleDark: '#4B2995',
    purple: '#8047F8',
    purpleLight: '#EBE5F9',
    yellowDark: '#C47F17',
    yellow: '#DBAC2C',
    yellowLight: '#F1E9C9'
  },
  spacings: {
    xxsmall: '0.8rem',
    xsmall: '1.6rem',
    small: '2.4rem',
    medium: '3.2rem',
    large: '4.0rem',
    xlarge: '4.8rem',
    xxlarge: '5.6rem'
  }
} as const
