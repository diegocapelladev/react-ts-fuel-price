import { Route, Routes } from 'react-router-dom'
import { Fuel } from './components/Fuel'

export const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Fuel />} />
    </Routes>
  )
}
